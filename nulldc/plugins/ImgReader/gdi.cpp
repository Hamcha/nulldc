#include "gdi.h"

Disc* load_gdi(wchar* file_) {
	char file[512];
	u32 iso_tc;
	Disc* gdiDisc = new Disc();

	size_t result;
	wcstombs_s(&result, file, 512, file_, 512);
	//memset(&gdi_toc,0xFFFFFFFF,sizeof(gdi_toc));
	//memset(&gdi_ses,0xFFFFFFFF,sizeof(gdi_ses));
	FILE* t;
	int err = fopen_s(&t, file, "rb");
	if (err != 0)
		return 0;
	fscanf_s(t, "%u\r\n", &iso_tc);
	printf("\nGDI : %u tracks\n", iso_tc);

	char temp[512];
	char path[512];
	strcpy_s(path, 512, file);
	size_t len = strlen(file);
	while (len > 2) {
		if (path[len] == '\\')
			break;
		len--;
	}
	len++;
	char* pathptr = &path[len];
	u32 TRACK = 0, FADS = 0, CTRL = 0, SSIZE = 0;
	s32 OFFSET = 0;
	for (u32 i = 0; i < iso_tc; i++) {
		//TRACK FADS CTRL SSIZE file OFFSET
		fscanf_s(t, "%u %u %u %u", &TRACK, &FADS, &CTRL, &SSIZE);
		//%s %d\r\n,temp,&OFFSET);
		//gdiDisc->tracks.push_back(
		while (iswspace(fgetc(t)));
		fseek(t, -1, SEEK_CUR);
		if (fgetc(t) == '"') {
			char c;
			int j = 0;
			while ((c = fgetc(t)) != '"')
				temp[j++] = c;
			temp[j] = 0;
		} else {
			fseek(t, -1, SEEK_CUR);
			fscanf_s(t, "%s", temp, 512);
		}

		fscanf_s(t, "%d\r\n", &OFFSET);
		printf("file[%u] \"%s\": FAD:%u, CTRL:%u, SSIZE:%u, OFFSET:%d\n", TRACK, temp, FADS, CTRL, SSIZE, OFFSET);



		Track track;
		track.ADDR = 0;
		track.StartFAD = FADS + 150;
		track.EndFAD = 0;		//fill it in
		track.file = 0;

		if (SSIZE != 0) {
			strcpy_s(pathptr, len, temp);
			FILE* trackFile;
			fopen_s(&trackFile, path, "rb");
			track.file = new RawTrackFile(trackFile, OFFSET, track.StartFAD, SSIZE);
		}
		gdiDisc->tracks.push_back(track);
	}

	gdiDisc->FillGDSession();

	return gdiDisc;
}


Disc* gdi_parse(wchar* file) {
	size_t len = wcslen(file);
	if (len > 4) {
		if (_tcsicmp(&file[len - 4], L".gdi") == 0) {
			return load_gdi(file);
		}
	}
	return 0;
}

void iso_term() {}