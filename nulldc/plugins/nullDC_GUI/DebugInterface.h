#pragma once


class DebugInterface
{
public:
	virtual wchar *disasm(unsigned int) {return L"NODEBUGGER";}
	virtual int getInstructionSize(int) {return 1;}

	virtual bool isBreakpoint(unsigned int) {return false;}
	virtual void setBreakpoint(unsigned int){}
	virtual void clearBreakpoint(unsigned int){}
	virtual void clearAllBreakpoints() {}
	virtual void toggleBreakpoint(unsigned int){}
	virtual unsigned int readMemory(unsigned int){return 0;}
	virtual unsigned int getPC() {return 0;}
	virtual void setPC(unsigned int) {}
	virtual void step() {}
	virtual void runToBreakpoint() {}
	virtual int getColor(unsigned int){return 0xFFFFFFFF;}
	virtual wchar *getDescription(unsigned int) {return L"";}
};
