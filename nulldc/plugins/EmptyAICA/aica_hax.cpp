#include "aica_hax.h"
#include "aica_hle.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

u8 *aica_reg;
u8 *aica_ram;

u32 __fastcall ReadMem_reg(u32 addr, u32 size) {
	ReadMemArrRet(aica_reg, addr & 0x7FFF, size);

	//should never come here
	return 0;
}

void __fastcall  WriteMem_reg(u32 addr, u32 data, u32 size) {
	if ((addr & 0xFFFF) == 0x2c00) {
		printf("Write to ARM reset, value= %x\n", data);
		if ((data & 1) == 0) {
			ARM_Katana_Driver_Info();
			arm7_on = true;
		} else {
			arm7_on = false;
		}
	}
	WriteMemArrRet(aica_reg, addr & 0x7FFF, data, size);

	//should never come here
}

u32 ReadMem_ram(u32 addr, u32 size) {
	//TODO : Add Warn
	return 0;
}

void WriteMem_ram(u32 addr, u32 data, u32 size) {
	switch (size) {
	case 1:
		aica_ram[addr&AICA_MEM_MASK] = (u8) data;
		break;
	case 2:
		*(u16*) &aica_ram[addr&AICA_MEM_MASK] = (u16) data;
		break;
	case 4:
		*(u32*) &aica_ram[addr&AICA_MEM_MASK] = data;
		break;
	}
}

int calls = 0;

void __fastcall UpdateAICA(u32 Cycles) {
	calls = 0;
	if (calls & 0x100 && HleEnabled()) {
		memset(&aica_ram[aud_drv->cmdBuffer& AICA_MEM_MASK], 0xFFFFFF, 0x8000);
	}

	*(u32*) &aica_ram[((0x80FFC0 - 0x800000) & 0x1FFFFF)] = *(u32*) &aica_ram[((0x80FFC0 - 0x800000) & 0x1FFFFF)] ? 0 : 3;

	//the kos command list is 0x810000 to 0x81FFFF
	//here we hack the first and last comands
	//seems to fix everything ^^
	*(u32*) &aica_ram[0x1000C] = 0x1;

	*(u32*) &aica_ram[0x1FFFC] = 1;
	//return 0x1;
	//hack kos command que

	for (int i = 0x100; i < 0x300; i += 4) {
		*(u32*) &aica_ram[i] = 0x800000;
	}

	*(u32*) &aica_ram[0x32040] = 0xFFFFFF;
	//return 0xFFFFFF;			//hack katana command que
	*(u32*) &aica_ram[0x32060] = 0xFFFFFF;
	//return 0xFFFFFF;			//hack katana command que

	//hack Katana wait
	//Waits for something , then writes to the value returned here
	//Found in Roadsters[also in Toy cmd]
	//it seems it is somehow rlated to a direct mem read after that 
	// im too tired to debug and find the actualy relation but it is
	*(u32*) &aica_ram[0x000EC] = 0x80000;
	//return 0x80000;
	*(u32*) &aica_ram[0x0005C] = 0x80000;
	//return 0x80000;

	//same as above , doa2/le
	*(u32*) &aica_ram[0x000E8] = 0x80000;

	for (int i = 0x014e0; i < 0x015e0; i += 4) {
		*(u32*) &aica_ram[i] = 0xFFFFFF;
	}

}
void init_mem() {
	memset(aica_ram, 0, AICA_MEM_SIZE);
	aica_reg = (u8*) malloc(0x8000);
	if (aica_reg > 0)
		memset(aica_reg, 0, 0x8000);
}

void term_mem() {
	free(aica_reg);
	//free(aica_ram);
}