// nullAICA.cpp : Defines the entry point for the DLL application.

#include "EmptyAICA.h"
#include "aica_hax.h"
#include "aica_hle.h"

#include <windows.h>

bool APIENTRY DllMain(HMODULE hModule,
					  DWORD  ul_reason_for_call,
					  LPVOID lpReserved
					  ) {
	return TRUE;
}


void cfgdlg(PluginType type, void* window) {
	//printf("null AICA plugin [h4x0rs only kthx]:No config kthx , however i will show ya some hle info :D\n");
	//MessageBox((HWND)window,"Nothing to configure","nullAICA plugin",MB_OK | MB_ICONINFORMATION);
	//ARM_Katana_Driver_Info();
}

s32 __fastcall PluginLoad(emu_info* param) {
	return rv_ok;
}

void __fastcall PluginUnload() {}

s32 __fastcall Init(aica_init_params* param) {
	aica_ram = param->aica_ram;
	init_mem();
	InitHLE();

	return rv_ok;
}
void __fastcall Term() {
	term_mem();
	TermHLE();
}
void __fastcall Reset(bool Manual) {
	ResetHLE();
}

//Give to the emu pointers for the PowerVR interface
void __stdcall dcGetInterface(plugin_interface* info) {
	common_info& common = info->common;
	aica_plugin_if& aica = info->aica;

	info->InterfaceVersion = PLUGIN_I_F_VERSION;

	wcscpy_s(common.Name, 128, L"Empty Aica Plugin [no sound/reduced compat] [" _T(__DATE__) L"]");

	common.InterfaceVersion = AICA_PLUGIN_I_F_VERSION;
	common.Type = Plugin_AICA;

	common.Load = PluginLoad;
	common.Unload = PluginUnload;

	aica.Init = Init;
	aica.Reset = Reset;
	aica.Term = Term;

	aica.Update = UpdateAICA;

	aica.ReadMem_aica_reg = ReadMem_reg;
	aica.WriteMem_aica_reg = WriteMem_reg;
}