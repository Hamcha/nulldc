#pragma once

#define key_CONT_C  (1 << 0)
#define key_CONT_B  (1 << 1)
#define key_CONT_A  (1 << 2)
#define key_CONT_START  (1 << 3)
#define key_CONT_DPAD_UP  (1 << 4)
#define key_CONT_DPAD_DOWN  (1 << 5)
#define key_CONT_DPAD_LEFT  (1 << 6)
#define key_CONT_DPAD_RIGHT  (1 << 7)
#define key_CONT_Z  (1 << 8)
#define key_CONT_Y  (1 << 9)
#define key_CONT_X  (1 << 10)
#define key_CONT_D  (1 << 11)
#define key_CONT_DPAD2_UP  (1 << 12)
#define key_CONT_DPAD2_DOWN  (1 << 13)
#define key_CONT_DPAD2_LEFT  (1 << 14)
#define key_CONT_DPAD2_RIGHT  (1 << 15)

#define key_CONT_ANALOG_UP  (1 << 16)
#define key_CONT_ANALOG_DOWN  (1 << 17)
#define key_CONT_ANALOG_LEFT  (1 << 18)
#define key_CONT_ANALOG_RIGHT  (1 << 19)
#define key_CONT_LSLIDER  (1 << 20)
#define key_CONT_RSLIDER  (1 << 21)

typedef signed __int8  s8;
typedef unsigned __int8  u8;
typedef unsigned __int16  u16;
typedef unsigned __int32 u32;

enum NAOMI_KEYS {
	NAOMI_SERVICE_KEY_1 =	1 << 0,
	NAOMI_TEST_KEY_1 =		1 << 1,
	NAOMI_SERVICE_KEY_2 =	1 << 2,
	NAOMI_TEST_KEY_2 =		1 << 3,

	NAOMI_START_KEY =		1 << 4,

	NAOMI_UP_KEY =			1 << 5,
	NAOMI_DOWN_KEY =		1 << 6,
	NAOMI_LEFT_KEY =		1 << 7,
	NAOMI_RIGHT_KEY =		1 << 8,

	NAOMI_BTN0_KEY =		1 << 9,
	NAOMI_BTN1_KEY =		1 << 10,
	NAOMI_BTN2_KEY =		1 << 11,
	NAOMI_BTN3_KEY =		1 << 12,
	NAOMI_BTN4_KEY =		1 << 13,
	NAOMI_BTN5_KEY =		1 << 14,
	NAOMI_COIN_KEY =		1 << 15,
};

struct joy_init_resp {
	u32 ratio;
	u32 mode;
	u32 players;
};

struct joy_init {
	u32 version;
	char name[512];
	u32 port;
};

struct joy_substate {
	u16 state;
	s8 jy;
	s8 jx;
	u8 r;
	u8 l;
};
struct joy_state {
	u32 id;
	joy_substate  substates[8];
};

struct _joypad_settings_entry {
	u8 KC;
	u32 BIT;
	wchar_t* name;
};
#define D(x) x ,_T( #x)